import time, threading as th, os, psutil
from time_tracker import time_stop, time_resume, is_over, __IS_END
from config import __SETTINGS
from colorama import Fore as fr
from files import log_session, toggle_autorun
from winsound import Beep

# Thread for checking.
__USER_CHECK_THREAD : th.Thread
# Event for the end.
time_to_end : th.Event = th.Event()

# Initialize the thread.
def init() -> None:
    if not __SETTINGS["AUTOMATIC"]:
        return

    global __USER_CHECK_THREAD

    __USER_CHECK_THREAD = th.Thread(target=is_lockstation_locked)
    __USER_CHECK_THREAD.start()

# Main thread function.
def is_lockstation_locked() -> None:
    while not time_to_end.is_set():
        i_am_here = not is_locked()
        if i_am_here:
            time_resume(False)
        else:
            time_stop(False)
        time.sleep(1)
        if i_am_here and is_over():
            print(f"\n{fr.GREEN}Workday is over. Closing...{fr.RESET}")
            Beep(1500, 500)
            if __SETTINGS["LOG_ON_EXIT"]:
                log_session(True)
            toggle_autorun(__SETTINGS["AUTORUN"])
            os._exit(0)

# Is station actually locked?
def is_locked() -> bool:
    is_locked : bool = False
    for proc in psutil.process_iter():
        if proc.name() == "LogonUI.exe":
            is_locked = True
    return is_locked
