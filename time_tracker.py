from typing import List, Tuple
import datetime, threading as th
from config import __SETTINGS
from colorama import Fore as fr

# Represents the time script was run.
__STARTUPTIME : datetime.datetime
# Represents estimate end time.
__ENDTIME : datetime.datetime
# Represents time spent on break.
__ON_BREAK : datetime.timedelta = datetime.timedelta()
# Represents the last time key (if any).
__LAST_TIME_KEY : datetime.datetime
# List of pairs of time keys.
__TIME_STAMPS : List[Tuple[datetime.datetime, datetime.datetime, str]] = []

# Workday length.
__WORKDAY : datetime.timedelta = datetime.timedelta(hours=6)

# Represents when the time was stopped.
__WHEN_STOPPED : datetime.datetime

# Is the timer stopped?
__STOPPED : bool = False
# Are we finalizing?
__IS_END : bool = False


# Runs once when the script starts.
def init() -> None:
    global __STARTUPTIME
    global __ENDTIME
    global __LAST_TIME_KEY

    __STARTUPTIME = datetime.datetime.now()
    __ENDTIME = __STARTUPTIME + __WORKDAY
    __LAST_TIME_KEY = __STARTUPTIME

# Emulates pause. Writes when the user stopped.
def time_stop(is_user: bool = True) -> None:
    if is_user and __SETTINGS["AUTOMATIC"]:
        print(f"{fr.RED}WARNING!{fr.RESET} You're in automatic mode.")

    global __STOPPED
    global __WHEN_STOPPED
    
    if not __STOPPED:
        __WHEN_STOPPED = datetime.datetime.now()
        add_time_key("Break.")
        __STOPPED = True
        print("Time stopped.")

# Emuates resume. Calculates the difference between now and the last stop
# and adds it to the __ENDTIME.
def time_resume(is_user: bool = True) -> None:
    if is_user and __SETTINGS["AUTOMATIC"]:
        print(f"{fr.RED}WARNING!{fr.RESET} You're in automatic mode.")

    global __STOPPED
    global __WHEN_STOPPED
    global __ENDTIME
    global __ON_BREAK

    if __STOPPED:
        __LAST_TIME_KEY = datetime.datetime.now()
        __ENDTIME += __LAST_TIME_KEY - __WHEN_STOPPED
        __ON_BREAK += __LAST_TIME_KEY - __WHEN_STOPPED
        __STOPPED = False
        print("Time resumed.")

# Prints how much time is already spent.
def time_spent() -> None:
    if __STOPPED:
        print(f"{fr.RED}WARNING!{fr.RESET} Time is stopped.")
    print(f"Time spent: {_format(datetime.datetime.now() - __STARTUPTIME, True)}")

# Is workday over?
def is_over() -> bool:
    return (not __STOPPED) and (datetime.datetime.now() - __STARTUPTIME + __ON_BREAK >= __WORKDAY)

# Prints how much time is left yet.
def time_left() -> None:
    if __STOPPED:
        print(f"{fr.RED}WARNING!{fr.RESET} Time is stopped.")
    print(f"Time left: {_format(__ENDTIME - datetime.datetime.now(), True)}")

# Add time key. TODO: optional string description.
def add_time_key(*comment : str) -> None:
    if __STOPPED:
        print(f"{fr.RED}WARNING!{fr.RESET} Time is stopped. Key is not set.")
        return

    global __LAST_TIME_KEY
    global __TIME_STAMPS

    __TIME_STAMPS.append((__LAST_TIME_KEY, datetime.datetime.now(), " ".join(comment)))
    __LAST_TIME_KEY = __TIME_STAMPS[-1][1]

# Print current time keys. TODO: option string description.
def print_time_keys() -> None:
    strings : List[str] = _gen_str_list(False)
    for string in strings:
        print(string, end='')

# Local function to format time.
def _format(input_time : datetime.timedelta, for_console: bool = False) -> str:
    if for_console:
        return fr.MAGENTA + "{:d}h {:02d}m".format(int(input_time.seconds // 3600), int(input_time.seconds // 60 % 60)) + fr.RESET
    else:
        return "{:d}h {:02d}m".format(int(input_time.seconds // 3600), int(input_time.seconds // 60 % 60))

# Local function to generate output to file or terminal.
def _gen_str_list(is_end : bool, for_console : bool = False) -> List[str]:
    result : List[str] = [f"{datetime.datetime.today()}\n", "=================================\n\n"]
    sum_val : datetime.timedelta = datetime.timedelta(0) 
    for val in __TIME_STAMPS:
        result.append(f"{val[0].hour}:{val[0].minute:02d} -> {val[1].hour}:{val[0].minute:02d} => {_format(val[1] - val[0], for_console)}   {val[2]}\n")
        sum_val += val[1] - val[0]
    if not is_end:
        result.append(f"{__LAST_TIME_KEY.hour}:{__LAST_TIME_KEY.minute:02d} ->\n\n")
    result.append(f"TOTAL: {_format(sum_val, for_console)}\n")
    return result
