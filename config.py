from typing import Dict, Callable, Tuple
import configparser as cp
import distutils.util as utils
from colorama import Fore as fr

# Relative config path.
__CONFIG_PATH : str = "config.ini"
# Dictionary of settings.
__SETTINGS : Dict[str, bool] = {
    "LOG_ON_EXIT" : False,
    "AUTORUN" : False,
    "AUTOMATIC" : True,
}

# Run to initialize.
def init() -> None:
    global __SETTINGS

    config = cp.ConfigParser()
    config.read(__CONFIG_PATH)

    for key in config['script-settings']:
        __SETTINGS[key.upper()] = config.getboolean('script-settings', key)

# Update config value in file.
def __update_config(key: str, value) -> None:
    config = cp.ConfigParser()
    config.read(__CONFIG_PATH)

    config['script-settings'][key] = str(value)
    config.write(open(__CONFIG_PATH, "w"))

# Loop for interactive update of config values.
def manual_config_update() -> None:
    time_to_leave : bool = False

    print(f"Type {fr.GREEN}\"help\"{fr.RESET} to see available config commands.\n")

    def show() -> None:
        print(f"{fr.GREEN}Available commands:{fr.RESET}")
        for setting in __SETTINGS.items():
            print("{0[0]:15} : {0[1]}".format(setting))
    
    def update(key : str, new_value : str) -> None:
        if key not in __SETTINGS:
            print(f"{fr.RED}Invalid key.{fr.RESET}")
        else:
            __SETTINGS[key] = True if utils.strtobool(new_value) else False
            __update_config(key, new_value)
            print(f"{fr.CYAN}{key}{fr.RESET} was succesfully updates to {fr.GREEN}{__SETTINGS[key]}{fr.RESET}")

    def help_config() -> None:
        for command in __config_commands:
            print(f"{fr.CYAN}{command:10s}{fr.RESET} | {__config_commands[command][1]}")
        print()
    
    def finish() -> None:
        nonlocal time_to_leave
        time_to_leave = True 

    __config_commands : Dict[str, Tuple[Callable, str]] = {
        "show" : (show, "Prints current configuration."),
        "update" : (update, "Updates selected key. Usage: update <KEY> <VALUE>."),
        "help" : (help_config, "Prints available commands."),
        "finish" : (finish, "Quit to main loop."),
    }
    
    command : str
    while not time_to_leave:
        print(f"{fr.YELLOW}Enter config command{fr.RESET}: ", end='')
        command = input()
        command_args = command.split(' ')
        if command_args[0] in __config_commands:
            try:
                __config_commands[command_args[0]][0](*command_args[1::])
            except TypeError:
                print(f"{fr.RED}Invalid arguments.{fr.RESET}")
        else:
                print(f"{fr.RED}Unknown command.{fr.RESET}")
