import configparser, os
from typing import List, Dict, Callable
from datetime import datetime
from time_tracker import _gen_str_list
from config import __SETTINGS, __CONFIG_PATH

# Batch file creation.
__START_PATH : str = os.path.abspath(".")
__AUTORUN_PATH : str = "".join(["C:\\Users\\", os.getlogin(), "\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"])
__BATCH_TEXT : str = f"set \"root=cd /d \"{__START_PATH}\"\"\n%root%\ncls\n\"python\" \"main.py\""
__AUTORUN_FILE : str = __AUTORUN_PATH + "\\START.bat"

# Logs to file current session when script is finalized.
def log_session(is_end : bool = False) -> None:
    log_str : List[str] = _gen_str_list(is_end)

    partial_name : str = str(datetime.now()).replace(':', '-')

    if not os.path.exists("log"):
        os.mkdir("log")

    file_name : str = f"log/{partial_name}.txt"
    log_file = open(file_name, "w")
    log_file.writelines(log_str)
    log_file.close()

# Toggles autorun.
def toggle_autorun(is_on: bool) -> None:
    # If we're on Windows.
    # If autorun is on, delete file.
    if not is_on and os.path.exists(__AUTORUN_FILE):
        os.remove(__AUTORUN_FILE)
    # If autorun is off, create one.
    elif is_on and not os.path.exists(__AUTORUN_FILE):
        batch_file = open(__AUTORUN_FILE, "w")
        batch_file.write(__BATCH_TEXT)
        batch_file.close()

# Checks if config file exists, if not -- creates one.
def check_config() -> None:
    # if config file exists...
    if os.path.exists(__CONFIG_PATH):

        config = configparser.ConfigParser()
        config.read(__CONFIG_PATH)
        script_settings = config['script-settings']

        for setting in __SETTINGS:
            if setting in script_settings:
                __SETTINGS[setting] = bool(script_settings[setting])

    # if don't...
    else:
        config = configparser.ConfigParser()
        config['script-settings'] = {}
        for setting in __SETTINGS:
            config['script-settings'].update(( { setting : str(__SETTINGS[setting]) } ))
        config.write(open(__CONFIG_PATH, "w"))

# Delete all log files.
def clear_log() -> None:
    folder : str = 'log/'
    for the_file in os.listdir(folder):
        file_path : str = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)