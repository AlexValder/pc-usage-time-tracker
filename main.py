import os, time, colorama as color
import time_tracker as tracker, config
from typing import Dict, Callable, Tuple
from colorama import Fore as fr

import files as files_io
import automatic_thread as uc

# Clears the screen.
def clear_console() -> None:
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

# Printing out help.
def print_help() -> None:
    for command in COMMANDS:
        print(f"{fr.CYAN}{command:10s}{fr.RESET} | {COMMANDS[command][1]}")
    print()

# Some preparations before app exit.
def app_exit() -> None:
    if config.__SETTINGS["LOG_ON_EXIT"]:
        files_io.log_session(True)
    files_io.toggle_autorun(config.__SETTINGS["AUTORUN"])
    uc.time_to_end.set()
    exit()

# Available to user commands.
COMMANDS : Dict[str, Tuple[Callable, str]] = {
    "remains" : (tracker.time_left, "Prints out time left in format {hours}h {munites}m."),
    "spent" : (tracker.time_spent, "Prints out time spent in format {hours}h {minutes}m."),
    "stop" : (tracker.time_stop, "Pauses the timer."),
    "resume" : (tracker.time_resume, "Resumes the timer."),
    "add" : (tracker.add_time_key, "Adds new time key with additional description as a string."),
    "print" : (tracker.print_time_keys, "Prints out time keys and total time spent"),
    "config" : (config.manual_config_update, "Configurate the script."),
    "clear" : (clear_console, "Clears the console."),
    "clear_logs" : (files_io.clear_log, "Deletes all logs."),
    "log" : (files_io.log_session, "Log current session."),
    "help" : (print_help, "Prints available commands."),
    "exit" : (app_exit, "Closes the script."),
}


if __name__ == "__main__":
    files_io.check_config()
    config.init()
    tracker.init()
    uc.init()
    color.init()

    print(f"\nType {fr.GREEN}\"help\"{fr.RESET} to see available commands.\n")

    while True:
        print(f"{fr.YELLOW}Enter the command:{fr.RESET} ", end='')
        command = input()
        command_args = command.split(' ')
        if command_args[0] in COMMANDS:
            try:
                COMMANDS[command_args[0]][0](*command_args[1::])
            except TypeError:
                print(f"{fr.RED}Invalid arguments.{fr.RESET}")
        else:
            print(f"{fr.RED}Unknown command.{fr.RESET}")